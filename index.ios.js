//Index.ios.js - place code here for ios

// Import a library to help create a Component
import React from 'react';
import { AppRegistry, View } from 'react-native';
import Header from './src/components/Header';
import AlbumList from './src/components/AlbumList';
// Create a Component - JSX
const App = () => (
  <View style={{ flex: 1 }}>
    <Header headerText={'Albums'} />
    <AlbumList />
  </View>
);
// Render it to the device - only root component uses AppRegistry
AppRegistry.registerComponent('albums', () => App);
